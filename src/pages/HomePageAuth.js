import React from 'react';
import Navigator from './Auth/Navigator';
import Paperbase from './Auth/Paperbase';
import Header from './Auth/Header';

const HomePageAuth = () => {
    return (
        <div>
            <Navigator />
            <Header />
            <Paperbase />
        </div>
    );
}

export default HomePageAuth;
