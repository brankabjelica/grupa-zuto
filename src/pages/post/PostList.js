import React from 'react';
import Container from '@mui/material/Container';

import PostItem from './PostItem';
import EmptyComponent from '../../infrastructure/components/EmptyComponent';
import { LocalizationContext } from '../../context/LanguageContext';

const PostList = ({ posts, profileId }) => {
  const { t } = React.useContext(LocalizationContext);

  return (
    <Container>
      {posts.length === 0 && (
        <EmptyComponent
          text='Trenutno nemate ni jedan post.'
         
        />
      )}
      {posts.length !== 0 && (
        <React.Fragment>
          {posts.map((e) => (
            <PostItem
              post={e}
              profileId={profileId}
              key={e.id}
            />
          ))}
        </React.Fragment>
      )}
    </Container>
  );
};

export default PostList;
