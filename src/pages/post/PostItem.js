import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import moment from 'moment'

import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardActions from '@mui/material/CardActions';

import client from '../../apis/client';
import PostForm from '../forms/PostForm';
import { LocalizationContext } from '../../context/LanguageContext';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid black',
  boxShadow: 24,
  p: 1
};

const PostItem = ({ post }) => {
  const { t } = React.useContext(LocalizationContext);
  let navigate = useNavigate()

  const [showEdit, setShowEdit] = useState(false);
  const [showDelete, setShowDelete] = useState(false);

  const handleCloseEdit = () => setShowEdit(false);
  const handleShowEdit = () => setShowEdit(true);

  const handleCloseDelete = () => setShowDelete(false);
  const handleShowDelete = () => setShowDelete(true);

  const deletePost = async () => {
    try {
      await client.delete(`/profiles/${post.profile}/posts/${post.id}/`)

      alert('Uspesno ste obrisali post')

      navigate('/post')
      navigate(0)

    } catch (err) {

      if (err.response) {
        alert(JSON.stringify(err.response.data));
      }
    }
  };

  return (
    <>
      <Card sx={{ maxWidth: 345, margin: 2 }}>
        <CardContent>
          {post.created &&
            <Typography variant="body2" color="text.secondary">
              Kreirano: {moment(post.created).format('DD. MM. YYYY. - hh:mm:ss')}
            </Typography>
          }
          {post.modified !== post.created &&
            <Typography variant="body2" color="text.secondary">
              Modifikovano: {moment(post.modified).format('DD. MM. YYYY. - hh:mm:ss')}
            </Typography>
          }

          <Typography gutterBottom variant="h5" component="div">
            {post.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {post.text}
          </Typography>
          <CardActions>
            <Button onClick={handleShowEdit} color='primary'>
              {'Izmeni post'}
            </Button>
            <Button onClick={handleShowDelete} color='primary'>
              {'Obrisi post'}
            </Button>
          </CardActions>
        </CardContent>
      </Card>

      <Modal open={showEdit} onClose={handleCloseEdit} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <PostForm profileId={post.profile} postData={post} />
        </Box>
      </Modal>


      <Modal open={showDelete} onClose={handleCloseDelete} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          Da li ste sigurni da zelite da obrisete post?
          <Button onClick={deletePost} color='primary' variant='contained' style={{ backgroundColor: 'black' }}>
            {'DA'}
          </Button>
          <Button onClick={handleCloseDelete} color='primary' variant='contained' style={{ backgroundColor: 'black' }}>
            {'NE'}
          </Button>

        </Box>
      </Modal>

    </>
  );
};

export default PostItem;
