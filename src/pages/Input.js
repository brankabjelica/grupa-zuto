import { Button } from "@mui/material";
import axios from "axios";
import { state, setState } from 'react'

const Input = () => {
    state = {
        selectedFile: null
    }
    const fileSelectedHandler = event => {
        setState ({
            selectedFile: event.target.files[0]
        })
    }
    const fileUploadHandler = () => {
        axios.post('https://preqbackend.herokuapp.com/');
    }
    return (
        <div>
        <input type='file' onChange={fileSelectedHandler} />
        <Button onClick={fileUploadHandler}>Upload</Button>
        </div>
    )

}

export default Input