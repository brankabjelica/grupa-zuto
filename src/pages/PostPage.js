import React, { useState, useEffect } from 'react';

import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';

import client from '../apis/client'
import { LocalizationContext } from '../context/LanguageContext';
import PostList from './post/PostList'
import PostForm from './forms/PostForm';
import Spacer from '../infrastructure/components/Spacer';
import IconButton from '@mui/material/IconButton';
import Fingerprint from '@mui/icons-material/Fingerprint';
import { Typography } from '@mui/material';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Skeleton from '@mui/material/Skeleton';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid black',
  boxShadow: 24,
  p: 1
};


const PostPage = () => {
  const { t } = React.useContext(LocalizationContext);

  const [show, setShow] = useState(false);
  const [posts, setPosts] = useState();
  const [profileId, setProfileId] = useState();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  useEffect(() => {
    let isActive = true;
    const loadData = async () => {

      const profile = await client.get(`/profiles/`);
      const post = await client.get(`/profiles/${await profile.data[0].id}/posts/`);

      if (isActive) {
        setPosts(await post.data)
        setProfileId(await profile.data[0].id)
      }
    };

    if (isActive) loadData();

    return () => {
      isActive = false;
    };

  }, []);
  return (
    <>
        <Paper sx={{ backgroundColor: 'yellow'}}>
        <Box p={2}>

      <Spacer height={'10rem'} />
      <Grid
        container
        spacing={0}
        direction="row"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: '10vh' }}
      >
        <Typography variant="body1" color="black">
          OSTAVI KOMENTAR
        </ Typography>
        <IconButton onClick={handleShow} aria-label="fingerprint" color="secondary">
          <Fingerprint />
        </IconButton>
      </Grid>
      { posts ? (
      posts !== undefined &&
        <PostList posts={posts} profileId={profileId} />
       ) : 
       <Skeleton variant="rounded" sx={{ bgcolor: '#ccffcc' }} width={410} height={160} />}
      <Modal open={show} onClose={handleClose} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <PostForm profileId={profileId} />
        </Box>
      </Modal>
        </Box>
        </Paper>
    </>
  );
};

export default PostPage;