
import React, { useEffect } from 'react';

import { LocalizationContext } from '../context/LanguageContext';
import client from '../apis/client';

import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import ProfileForm from './forms/ProfileForm';
import { Paper } from '@mui/material';
import Card from './Auth/Hp/Card';
import CardMm from './Auth/Hp/CardMm';
import CardVs from './Auth/Hp/CardVs';
import CardDjM from './Auth/Hp/CardDjM';
import PostPage from './PostPage';
import TemporaryDrawer from './Auth/TemporaryDrawer';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid black',
    boxShadow: 24,
    p: 1
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'set_show_profile_modal':
            return { ...state, showProfileModal: action.payload };
        case 'set_profile_data':
            return { ...state, profileData: action.payload };
        case 'set_user':
            return { ...state, user: action.payload };
        default:
            return state;
    }
};


const HomePage = (props) => {
    const [state, dispatch] = React.useReducer(reducer, {
        showProfileModal: false,
        profileData: undefined,
        user: undefined
    });


    const { t } = React.useContext(LocalizationContext);
    
    useEffect(() => {
        let isActive = true;
        const loadData = async () => {

            const profile = await client.get(`/profiles/`);
            const user = await client.get(`/auth/users/me/`);

            if (isActive) {
                if (profile.data.length === 0) {
                    dispatch({ type: 'set_show_profile_modal', payload: true })

                    // useStateNacin
                    // setShowProfileModal(true);
                }
                else {

                    dispatch({ type: 'set_profile_data', payload: await profile.data[0] })
                    dispatch({ type: 'set_user', payload: await user.data.name })
                    dispatch({ type: 'set_show_profile_modal', payload: false })


                    //useState nacin
                    // setprofileData(await profile.data[0])
                    // setUser(await user.data.name)
                    // setShowProfileModal(false);
                }
            }
        };

        if (isActive) loadData();

        return () => {
            isActive = false;
        };

    }, []);



    return (
        <div style={{  marginLeft: '2rem', marginRight: '2rem' }}>
            
            <div>
                <PostPage />
            </div>

            <Modal open={state.showProfileModal} style={{ overflow: 'scroll' }}>
                <Box sx={style}>
                    <ProfileForm />
                </Box>
            </Modal>
            <Paper sx={{ backgroundColor: 'blue'}}>
        <Box p={2}>
            <div style={{ display: "flex" }} >
                <div style={{ margin: 5 }}>
                    <Card />
                </div>
                <div style={{ margin: 5 }}>
                    <CardMm />
                </div>
                <div style={{ margin: 5 }}>
                    <CardVs />
                </div>
                <div style={{ margin: 5 }}>
                    <CardDjM />
                </div>
            </div>
            </Box>
            </Paper>
        </div>

    );
}

export default HomePage;
