import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import moment from 'moment';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Paper } from '@mui/material';
import { LocalizationContext } from '../context/LanguageContext';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import client from '../apis/client';
import Spacer from '../infrastructure/components/Spacer';
import TemporaryDrawer from './Auth/TemporaryDrawer';
import Skeleton from '@mui/material/Skeleton';
import Input from './Input';

const ProfilePage = (props) => {
    const { t } = React.useContext(LocalizationContext);
    let navigate = useNavigate()

    const [profileData, setProfileData] = useState()
    const [user, setUser] = useState()

    useEffect(() => {
        let isActive = true;
        const loadData = async () => {

            const profile = await client.get(`/profiles/`);
            console.log('profili', profile)
            const user = await client.get(`/auth/users/me/`);



            if (isActive) {
                if (profile.data.length === 0) {
                    navigate('/')
                    navigate(0)

                }
                else {
                    setProfileData(await profile.data[0])
                    setUser(await user.data.name)
                }
            }
        };

        if (isActive) loadData();

        return () => {
            isActive = false;
        };

    }, []);


    return (
        <>
            <Paper sx={{ backgroundColor: 'yellow', maxHeight: 800 }}>
                <Box p={2}>
                    <Spacer height='4rem' />
                    <Container component='main' maxWidth='md'>
                        <Grid container spacing={3} direction='column'>
                            {(user !== undefined &&
                                user !== '') ? (<Grid item><Typography mt={3} variant='h4'>
                                    {`Zdravo ${user}`}
                                </Typography></Grid>) : null}
                        </Grid>

                        <Spacer height='3rem' />
                        {profileData ? (
                            <Grid>
                                <Grid>
                                    <Card sx={{ maxWidth: 345, bgcolor: '#ccffcc' }}>
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {user}
                                            </Typography>
                                            {(profileData !== undefined &&
                                                profileData.date_of_birth !== '') &&
                                                <Grid item><Typography variant='inherit'>Datum rodjenja: <span style={{ color: 'black' }}>{moment(profileData.date_of_birth).format('DD. MM. YYYY.')}</span></Typography></Grid>
                                            }
                                            {(profileData !== undefined &&
                                                profileData.city !== '') &&
                                                <Grid item><Typography variant='inherit'>Grad: <span style={{ color: 'black' }}>{profileData.city}</span></Typography></Grid>
                                            }
                                            {(profileData !== undefined &&
                                                profileData.phone !== '') &&
                                                <Grid item><Typography variant='inherit'>Telefon: <span style={{ color: 'black' }}>{profileData.phone}</span></Typography></Grid>
                                            }
                                            {(profileData !== undefined &&
                                                profileData.address !== '') &&
                                                <Grid item><Typography variant='inherit'>Adresa: <span style={{ color: 'black' }}>{profileData.address}</span></Typography></Grid>
                                            }
                                        </CardContent>
                                    </Card>
                                </Grid>
                            </Grid>) :
                            <Stack spacing={1}>
                                <Skeleton sx={{ bgcolor: 'grey.700' }} variant="circular" width={100} height={100} />
                                <Skeleton sx={{ bgcolor: 'grey.700' }} variant="rectangular" width={410} height={150} />
                            </Stack>
                        }
                        <Spacer height='2rem' />
                        <TemporaryDrawer />
                        <Input />
                    </Container>
                </Box>
            </Paper>
        </>
    );
}

export default ProfilePage;