import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import milanD from '../Hp/Img/milanD.jpg';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, backgroundColor: 'cadetblue' }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: '#ECDC0B' }} aria-label="recipe">
            M
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <AccountCircleOutlinedIcon />
          </IconButton>
        }
        title="Milan Denić"
        subheader="Diplomirani biolog"
      />
      <CardMedia
        component="img"
        height="300"
        image={milanD}
        alt="Milan Denić"
      />
      <CardContent>
        <Typography variant="body1" color="black">
          Milan Denić je čovek otvorenog uma spreman da uči nove stvari i spreman na promene. 
          Obavljao više različitih tipova poslova, a ponekada i u isto vreme. Od programerskih veština poznaje rad u HTML, CSS, JS, REACT, GIT.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="pokaži još"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>CV:</Typography>
          <Typography paragraph>
          24.09.2018. – 07.12.2018. : Republički zavod za statistiku, Srbija, Beograd, Administracija, radno mesto: Anketar u opštini Gadžin Han
          Opis posla: Anketiranje poljoprivrednih gazdinstava u okviru Istraživanja o strukturi poljoprivrednih gazdinstava prema dodeljenoj Listi i unošenje podataka u elektronski upitnik na laptopu na terenu.
          </Typography>
          <Typography paragraph>
          04.01.2017. – i dalje zaposlen: Opštinska uprava, Srbija, Gadžin Han, Administracija, Kabinet predsednika opštine, radno mesto: Upravljanje ljudskim resursima
          Opis posla: Učestvovanje u izradi kadrovskog plana, izrada kadrovske evidencije zaposlenih, briga o međuljudskim odnosima u OU, organizovanje obuka i treninga, povezivanje timova i ljudi, olakšavanje procesa u OU, odabir kandidata nakon raspisivanja konkursa, učestvovanje u ocenjivanju zaposlenih.
          </Typography>
          <Typography paragraph>
          20.01.2017. – 30.06.2017. : Osnovna Škola “Vitko i Sveta” , Srbija, Gadžin Han, Nauka i obrazovanje, Nastavnik biologije
          Opis posla: Priprema i sprovođenje časova biologije u školi
          </Typography>
          <Typography paragraph>
          01.06.2016 – 31.12.2016 : Opštinska uprava, Srbija, Gadžin Han, Administracija, Službenik u mesnoj kancelariji
          Opis posla: Unošenje podataka iz knjige umrlih, rođenih i venčanih u bazu podataka
          </Typography>
          <Typography paragraph>
          01.06.2015 – 29.02.2016 : Opštinska uprava, Lokalna poreska uprava, Srbija, Gadžin Han, Administracija, Popisivač imovine projekta “ Unapređenje funkcionisanja LPA i naplate poreza u Gadžinom Hanu ”
          Opis posla: Obilazak poreskih obveznika, uručenje rešenja za naplatu poreza i popunjavanje PP2 obrasca
          </Typography>
          <Typography paragraph>
          10.11.2014 – 09.04.2015 : Centar za socijalni rad opštine Gadžin Han, Srbija, Administracija, Službenik u centru za socijalni rad
          Opis posla: Pomoć stručnim radnicima prilikom obavljanja njihovih redovnih poslova kao što su fotokopiranje, radnje vezane za overu kopija, slanje pošte, rad na računaru.
          </Typography>
          <Typography paragraph>
          01.10.2013. - 30.11.2013.: Opštinska uprava, Srbija, Gadžin Han, Administracija, Službenik u mesnoj kancelariji
          Opis posla: Unošenje podataka iz knjige umrlih u bazu podataka
          </Typography>
          <Typography paragraph>
          01.02.2013. - 28.03.2013.: Osnovna Škola “Vitko i Sveta” , Srbija, Gadžin Han, Nauka i obrazovanje, Nastavnik biologije
          Opis posla: Priprema i sprovoðenje casova biologije u školi
          </Typography>
          <Typography paragraph>
          01.10.2012. - 16.12.2012.: Republički zavod za statistiku, Srbija, Beograd, Administracija, Opštinski instruktor u popisu poljoprivrede u opštini Gadžin Han
          Opis posla: Sprovođenje popisa poljoprivrede, nadgledanje rada i provera upitnika popisivaca. Redovno izveštavanje republickog instruktora o svom radu i radu popisivaca.
          </Typography>
          <Typography paragraph>
          01.09.2011. - 10.08.2012.: Osnovna Škola “Vitko i Sveta” , Srbija, Gadžin Han, Nauka i obrazovanje, Nastavnik biologije
          Opis posla: Priprema i sprovođenje časova biologije u školi
          </Typography>
          <Typography paragraph>
          01.10.2011. - 16.10.2011.: Republički zavod za statistiku, Srbija, Beograd, Administracija, Administracija, Opštinski instruktor u popisu stanovništva u opštini Gadžin Han
          Opis posla: Sprovođenje popisa stanovništva, nadgledanje rada i provera upitnika popisivača, redovno izveštavanje republickog instruktora.
          </Typography>
          <Typography paragraph>
          24.09.2010. - 23.11.2011.: Opštinska uprava, Srbija, Gadžin Han, Administracija, Projektni službenik na projektu Exchange 3 SWAM
          Opis posla: Monitoring i kontrola procesa čišćenja deponija koje se odnose na projekat Exchange 3 "SWAM Održivo upravljanje otpadom u opštini Gadžin Han". Uska saradnja sa kompanijom i radnicima koji ce cistiti deponije. Rad u koordinaciji sa ostalim zaposlenima u cilju efikasnog obavljanja projektnih aktivnosti, priprema, organizacija i koordinacija radnji vezane za projekat: treninzi, radionice, seminari, konferencije sastanci. Asistiranje pri izradi publikacija, letaka, informativnih tabli, kontaktiranje saradnika i korisnika projekta.
          </Typography>
          <Typography paragraph>
          20.09.2010. - 08.10.2010.: Osnovna škola "Ivan Goran Kovačić", Srbija, Niška Banja, Nauka i obrazovanje, Nastavnik biologije
          Opis posla: Priprema i sprovođenje časova biologije u školi.
          </Typography>
          <Typography paragraph>
          15.09.2010. - 15.05.2011.: Ministarstvo Poljoprivrede Republike Srbije, Srbija, Gadžin Han, Administracija, Regionalni koordinator za ruralni razvoj
          Opis posla: Obilazak poljoprivrednih proizvođaca i njihovo informisanje o novim uredbama, držanje radionica, rad sa strankama u kancelariji.
          </Typography>
          <Typography paragraph>
          02.02.2010. - 12.02.2010.: Osnovna škola "Bubanjski Heroji", Srbija, Niš, Nauka i obrazovanje, Nastavnik biologije
          Opis posla: Priprema i sprovođenje časova biologije u školi.
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
