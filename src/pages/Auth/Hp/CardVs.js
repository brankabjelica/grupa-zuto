import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import VladimirS from '../Hp/Img/VladimirS.jpg';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, backgroundColor: 'cadetblue' }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: '#ECDC0B' }} aria-label="recipe">
            V
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <AccountCircleOutlinedIcon />
          </IconButton>
        }
        title="Vladimir Simović"
        subheader="Master mašinstva"
      />
      <CardMedia
        component="img"
        height="300"
        image={VladimirS}
        alt="Vladimir Simović"
      />
      <CardContent>
        <Typography variant="body1" color="black">
        U zivotu se uglavnom bavio hardwareom. Imao svoje preduzece a radio je i u JP. Rukovodio velikim brojem ljudi i bio odgovoran za mnoge stvari.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="pokaži još"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>CV:</Typography>
          <Typography paragraph>
          
          </Typography>
          <Typography paragraph>
          
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
