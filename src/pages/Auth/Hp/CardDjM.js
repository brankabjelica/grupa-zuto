import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Djole from '../Hp/Img/Djole.jpg';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, backgroundColor: '#78BDB3' }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: '#ECDC0B' }} aria-label="recipe">
            Đ
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <AccountCircleOutlinedIcon />
          </IconButton>
        }
        title="Đorđe Majkić"
        subheader="Web dizajner"
      />
      <CardMedia
        component="img"
        height="300"
        image={Djole}
        alt="Đorđe Majkić"
      />
      <CardContent>
        <Typography variant="body1" color="black">
          Radim privatno kao grafički i veb dizajner što je i moja profesija. Sa radom sam počeo nakon završene IT akademije, a trenutno sam u potrazi za stalnim poslom.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="pokaži još"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>CV:</Typography>
          <Typography paragraph>
            Edukacija:
          </Typography>
          <Typography paragraph>
            Ekonomska škola “Đura Jakšić – Srpska Crnja”
            2014. – 2018.
          </Typography>
          <Typography paragraph>
            IT Academy (Design & Multimedia)
            2019. – 2020.
          </Typography>
          <Typography paragraph>
            Radno iskustvo:
          </Typography>
          <Typography paragraph>
            Freelancer.com Webpage creator (Samostalan rad)
            2016.–2018.
            Završavanje projekata na vreme.
            Odgovoran rad.
            Dobra komunikacija sa klijentima.
          </Typography>
          <Typography paragraph>
            UpWork Webpage creator (Samostalan rad)
            2019.– 2020.
            Odgovoran rad.
          </Typography>
          <Typography paragraph>
            Veštine:
          </Typography>
          <Typography paragraph>
            HTML
            CSS
            JavaScript
            Adobe Photoshop CC
            Adobe Illustrator CC
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
