import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import milanM from '../Hp/Img/milanM.png';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function RecipeReviewCard() {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, backgroundColor: '#78BDB3' }}>
      <CardHeader
        avatar={
          <Avatar sx={{ bgcolor: '#ECDC0B' }} aria-label="recipe">
            M
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <AccountCircleOutlinedIcon />
          </IconButton>
        }
        title="Milan Manić"
        subheader="Umetnik"
      />
      <CardMedia
        component="img"
        height="300"
        image={milanM}
        alt="Milan Denić"
      />
      <CardContent>
        <Typography variant="body1" color="black">
          Милан Манић је свестрани уметник. Више од двадесет година се бави мултимедијом. Последњих година, присутан је, како на ликовној, тако и на књижевној сцени савременог стваралаштва у земљи и иностранству.
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="pokaži još"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>CV:</Typography>
          <Typography paragraph>
            UMETNIČKI RAD:
            Милан Манић је ликовни уметник проширених медија. Члан је УЛУС-а и још николико уметничких удружења. Имао је пет самосталних изложби и учествовао на 80 групних изложби у земљи и иностранству: БиХ, Северна Македонија, Кипар, Бугарска, Мађарска, Бразил, Турска, Кина. Награђиван.
            Пише кратке приче и песме; објавио је збирку кратких прича и песама „Све те преврталице“ (Пирот: Pi-press, 2008); објавио је збирку песама „Сумпор“ (Издавач Народна библиотека Пирот: 2022.); заступљен у бројним зборницима и две антологије; награђиван; сарађује са домаћим и страним часописима: објављивао у Црној Гори, БиХ и Хрватско
          </Typography>
          <Typography paragraph>
          OBRAZOVANJE:
            2005                Univerzitet u Nišu, PMF
            Specijalistička informatička obuka – Photoshop

            1998-2000        Univerzitet u Nišu, Elektronski fakultet
            Osnovne studije iz elektrotehnike, opšti smer

            1994-1998        Tehničla škola Pirot
            Elektrotehničar automatike
          </Typography>
          <Typography paragraph>
           VEŠTINE:
            •	HTML, CSS, JS
            •	Git, GitLab
            •	WordPress
            •	React.js
            •	Photoshop
          </Typography>
          <Typography paragraph>
            RADNO ISKUSTVO

            2017-2021        Samostalni umetnik, član ULUS
            Osoba sa statusom samostalnog umetnika

            4.-10. 2012       Instruktor, Sportski centar Pirot
            Instruktor i trener u teretani

            2007-2009        Novinar, Radio+ Pirot
            Voditelj autorske emisije i reporter

          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
