import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { LocalizationContext } from '../../context/LanguageContext';
import { Link } from "react-router-dom";

export default function SimpleBackdrop() {
  const { t } = React.useContext(LocalizationContext);
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Link to="/register" ><Button onClick={handleToggle}>{t('register')}</Button></Link>
      <Backdrop
        sx={{ color: '#F8D14C', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={open}
        onClick={handleClose}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}