import * as React from 'react';
import { useNavigate } from 'react-router-dom';

import * as yup from 'yup';
import { useFormik } from 'formik';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import SimpleBackdrop from './SimpleBackdrop';
import { LocalizationContext } from '../../context/LanguageContext';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { IconButton } from '@mui/material';
import { InputAdornment } from '@mui/material';

import client_auth from '../../apis/client_auth'




export default function SignIn() {
  const { t } = React.useContext(LocalizationContext);
  const navigate = useNavigate()

  const [error, setError] = React.useState('');

    const [showPassword, setShowPassword] = React.useState(false);
    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);

    const initialValues = {
      username: localStorage.getItem('username') ?? '',
      password: localStorage.getItem('password') ?? '',
      rememberMe: false
  }


  const onSignIn = async(values) => {
      try {
          const loginData = {
              username: values.username,
              password: values.password
          }


          const response = await client_auth.post('/auth/jwt/create/', loginData );

          const tok = JSON.stringify(response.data);
          const parsedData = JSON.parse(tok);
  
          localStorage.setItem('token', parsedData.access);
          localStorage.setItem('refreshToken', parsedData.refresh);
  
  
          if (values.rememberMe === true) {
              localStorage.setItem('username', values.username);
              localStorage.setItem('password', values.password);
          }
  
  
          navigate('/');
          navigate(0);
  
      } catch (error) {
          setError('Greska')
          return

      }
  }

  const validationSchema = yup.object({
      username: yup.string()
          .required('Obavezno polje'),
      password: yup.string()
          .required('Obavezno polje')
  });

  const formik = useFormik({
      initialValues: initialValues,
      validationSchema: validationSchema,
      onSubmit: onSignIn,
  });



  return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Stack>
          <Avatar sx={{ bgcolor: '#e8e826' }} variant="rounded">
      </Avatar>
    </Stack>
          <Typography component="h1" variant="h5">
          {t('login')}
          </Typography>
          <Box component="form" noValidate onSubmit={formik.handleSubmit} sx={{ mt: 3, boxShadow: 1 }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="username"
                  label={t('korisnicko')}
                  name="username"
                  value={formik.values.username}
                  onChange={formik.handleChange}
                  error={formik.touched.username && Boolean(formik.errors.username)}
                  helperText={formik.touched.username && formik.errors.username}

                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label={t('pass')}
                  type={showPassword ? 'text' : 'password'}
                  id="password"
                  autoComplete="new-password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  error={formik.touched.password && Boolean(formik.errors.password)}
                  helperText={formik.touched.password && formik.errors.password}
                  InputProps={{
                    endAdornment: (
                        <InputAdornment position='end'>
                            <IconButton
                                aria-label='toggle password visibility'
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                            >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    )
                }}
                />
              </Grid>
              
            </Grid>
            <Button
              type="submit"
              fullWidth
              sx={{ mt: 3, mb: 2, color: 'black', backgroundColor: '#e8e826', borderColor: 'yellow' }}
            >
              {t('dugme')}
            </Button>
            <Grid container justifyContent="flex-end">
              <SimpleBackdrop />
            </Grid>
          </Box>
        </Box>
      </Container>
  );
}
