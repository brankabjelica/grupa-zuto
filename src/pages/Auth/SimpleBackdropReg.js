import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { LocalizationContext } from '../../context/LanguageContext';
import { Link } from "react-router-dom";


export default function SimpleBackdropReg() {
  const { t } = React.useContext(LocalizationContext);
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Link to="/login" ><Button onClick={handleToggle}>{t('buttonRegister')}</Button></Link>
      <Backdrop
        sx={{ color: '#F8D14C', zIndex: (theme) => theme.zIndex.drawer + 1, transitionDelay: 10000 }}
        open={open}
        onClick={handleClose}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}