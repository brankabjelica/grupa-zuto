import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import Grid from '@mui/material/Grid';
import { useState, useEffect } from 'react';
import { LocalizationContext } from '../../context/LanguageContext';
import { useNavigate } from 'react-router-dom';
import { Modal } from '@mui/material';
import ProfileForm from '../forms/ProfileForm';
import ChangePassword from '../forms/ChangePassword';
import DeleteAccount from '../forms/DeleteAccount';
import DeleteIcon from '@mui/icons-material/Delete';
import Edit from '@mui/icons-material/Edit';
import EditAttributes from '@mui/icons-material/EditAttributes';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  border: '2px solid black',
  boxShadow: 24,
  p: 1
};


export default function TemporaryDrawer() {
  const [state, setState] = React.useState({
    bottom: false
  });
  const { t } = React.useContext(LocalizationContext);
  let navigate = useNavigate()

  const [profileData, setProfileData] = useState()
  const [user, setUser] = useState()

  const [showChange, setShowChange] = useState(false);
  const [showDelete, setShowDelete] = useState(false);
  const [showEdit, setShowEdit] = useState(false);

  const handleCloseChange = () => setShowChange(false);
  const handleShowChange = () => setShowChange(true);

  const handleCloseDelete = () => setShowDelete(false);
  const handleShowDelete = () => setShowDelete(true);

  const handleCloseEdit = () => setShowEdit(false);
  const handleShowEdit = () => setShowEdit(true);
  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'MENI' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['Edit'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <Grid columns={{ xs: 4, md: 20 }}>
              <ListItemButton>

                <Grid item>
                  <Button onClick={handleShowEdit} color='primary' variant='contained' startIcon={<Edit />}>
                    {'Izmeni profil'}
                  </Button>
                </Grid>
              </ListItemButton>
              <ListItemButton>
                <Grid item>
                  <Button onClick={handleShowChange} color='primary' variant='contained' startIcon={<EditAttributes />}>
                    {'Izmeni lozinku'}
                  </Button>
                </Grid>
              </ListItemButton>
              <ListItemButton>
                <Grid item>
                  <Button onClick={handleShowDelete} color='primary' variant='contained' startIcon={<DeleteIcon />}>
                    {'Obrisi nalog'}
                  </Button>
                </Grid>
              </ListItemButton>
            </Grid>
          </ListItem>
        ))}
      </List>
    </Box >
  );

  return (
    
    <div>

      {['MENI'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)} variant='contained'>{anchor}</Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
      <Modal open={showChange} onClose={handleCloseChange} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <ChangePassword />
        </Box>
      </Modal>
      <Modal open={showEdit} onClose={handleCloseEdit} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <ProfileForm profileData={profileData} />
        </Box>
      </Modal>
      <Modal open={showDelete} onClose={handleCloseDelete} style={{ overflow: 'scroll' }}>
        <Box sx={style}>
          <DeleteAccount />
        </Box>
      </Modal>
    </div>
    
  );
}
