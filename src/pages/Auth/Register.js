import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import SimpleBackdropReg from './SimpleBackdropReg';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { LocalizationContext } from '../../context/LanguageContext';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { IconButton } from '@mui/material';
import { InputAdornment } from '@mui/material';
import client_auth from '../../apis/client_auth';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

const initialValues = {
  name: '',
  username: '',
  email: '',
  password: '',
  re_password: '',
}


export default function Register() {
    const { t } = React.useContext(LocalizationContext);

    const [showPassword, setShowPassword] = React.useState(false);
    const handleClickShowPassword = () => setShowPassword(!showPassword);
    const handleMouseDownPassword = () => setShowPassword(!showPassword);

    const [showrePassword, setShowrePassword] = React.useState(false);
    const handleClickShowrePassword = () => setShowrePassword(!showrePassword);
    const handleMouseDownrePassword = () => setShowrePassword(!showrePassword);

    const navigate = useNavigate()

    const [error, setError] = React.useState('');

    const onSignUp = async(values) => {
    
      try {
          const newUser = {
              name: values.name,
              username: values.username,
              email: values.email,
              password: values.password,
              re_password: values.re_password,
          };

          console.log(newUser)

          const reg = await client_auth.post(`/auth/users/`, newUser)

         if (reg.status === 201) {
          alert('Uspesno ste se registrovali')
          navigate('/login')
          navigate(0)
         }


      } catch (error) {
          setError('Greska')
          return

      }
  }
  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: onSignUp
  })
  return (

    
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ bgcolor: '#e8e826' }} variant="rounded">
        </Avatar>
          <Typography component="h1" variant="h5">
          {t('signUp')}
          </Typography>
          <form onSubmit={formik.handleSubmit}>
          <Box sx={{ mt: 3, boxShadow: 1 }} >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  autoComplete="given-name"
                  name="name"
                  required
                  fullWidth
                  id="name"
                  label={t('name')}
                  autoFocus
                  value={formik.values.name}
                  onChange={formik.handleChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="username"
                  label={t('korisnicko')}
                  name="username"
                  autoComplete="Username"
                  value={formik.values.username}
                  onChange={formik.handleChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label={t('email')}
                  name="email"
                  autoComplete="email"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label={t('password')}
                  type={showPassword ? 'text' : 'password'}
                  id="password"
                  autoComplete="new-password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  InputProps={{
                    endAdornment: (
                        <InputAdornment position='end'>
                            <IconButton
                                aria-label='toggle password visibility'
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                            >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    )
                }}

                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="re_password"
                  label={t('retype')}
                  type={showrePassword ? 'text' : 'password'}
                  id="re_password"
                  autoComplete="retype-password"
                  value={formik.values.re_password}
                  onChange={formik.handleChange}
                  InputProps={{
                    endAdornment: (
                        <InputAdornment position='end'>
                            <IconButton
                                aria-label='toggle password visibility'
                                onClick={handleClickShowrePassword}
                                onMouseDown={handleMouseDownrePassword}
                            >
                                {showrePassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    )
                }}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" backgroundcolor="yellow" />}
                  label={t('notification')}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              sx={{ mt: 3, mb: 2, color: 'black', backgroundColor: '#e8e826', borderColor: 'yellow' }}
            >
              {t('signUp')}
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <SimpleBackdropReg />
              </Grid>
            </Grid>
          </Box>
          </form>
        </Box> 
      </Container>
  );
        }