import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import { Link } from "react-router-dom";
import { Button } from '@mui/material';

import './LanguageDropdown.css';

const LanguageDropdown = () => {
  console.disableYellowWarnin = true;
  const [locale, setLocale] = React.useState(localStorage.getItem('language'));

  const handleSetLocale = (value) => {
    setLocale(value);
    localStorage.setItem('language', value);
    window.location.reload()
  };

  return (
    <>
      <div className='bg_top_nav'>
      <Link to="/" ><Button><HomeIcon className="home" /></Button></Link>
        <button
         className='button_lang'
          value='sr'
          onClick={(event) => handleSetLocale(event.target.value)}
        >
          lat
        </button>
        <button
          className='button_lang'
          value='cyr'
          onClick={(event) => handleSetLocale(event.target.value)}
        >
          ћир
        </button>
        <button
          className='button_lang'
          value='en'
          onClick={(event) => handleSetLocale(event.target.value)}
        >
          eng
        </button>

      </div>
    </>
  );
};

export default LanguageDropdown;
