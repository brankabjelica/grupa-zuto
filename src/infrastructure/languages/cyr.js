const cyr = {
    homePage: 'Хоме пејџ',
    submit: 'Потврди',
    login: 'Улогуј се',
    korisnicko: 'Корисничко име',
    pass: 'Лозинка',
    dugme: 'Улогуј се',
    register: 'Ако немаш налог, региструј се',
    signUp: 'Региструј се',
    email: 'Имејл',
    name: 'Име',
    password: 'Лозинка',
    retype: 'Поново укуцајте лозинку',
    notification: 'Желим да примам обавештења, рекламе и ажурирања преко имејла.',
    buttonRegister: 'Већ имате налог? Улогуј се'

};

export default cyr;
