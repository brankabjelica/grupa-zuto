const eng = {
    homePage: 'Home Page',
    submit: 'Submit',
    login: 'Login',
    korisnicko: 'Username',
    pass: 'Password',
    dugme: 'Login',
    register: 'Dont have account? Register',
    signUp: 'Sign up',
    email: 'Email',
    name: 'First name',
    password: 'Password',
    retype: 'Retype password',
    notification: 'I want to receive inspiration, marketing promotions and updates via email.',
    buttonRegister: 'Already have account? Log in'
};

export default eng;
