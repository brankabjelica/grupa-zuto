const sr = {
    homePage: 'Home pejdz',
    submit: 'Potvrdi',
    login: 'Uloguj se',
    korisnicko: 'Korisničko ime',
    pass: 'Lozinka',
    dugme: 'Uloguj se',
    register: 'Ako nemaš nalog, registruj se',
    signUp: 'Registruj se',
    email: 'Imejl',
    name: 'Ime',
    password: 'Lozinka',
    retype: 'Ponovo ukucajte lozinku',
    notification: 'Želim da primam obaveštenja, reklame i ažuriranja preko imejla.',
    buttonRegister: 'Već imate nalog? Uloguj se'
};

export default sr;
